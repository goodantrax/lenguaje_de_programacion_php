<?php 

	session_start();
	if(!isset($_SESSION['usuario'])){
		header("location: login.php");
	}
	$indice = $_REQUEST['indice'];
	$usuario = $_SESSION['usuario'];
	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>Info</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>
	<body class="d-flex flex-column vh-100">
		<div class="container mw-100 px-0">

			<nav class="navbar navbar-expand-lg navbar-dark bg-primary mw-100">
			  <div class="container-fluid">
			  	<?php
			        echo "<a class='navbar-brand' href='info.php?indice=$indice'>Home</a>"
			    ?>
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarText">
			      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
			        <li class="nav-item">
			          <?php
			          	echo "<a class='nav-link active' aria-current='page' href='formulario.php?indice=$indice'>Registrar Alumnos</a>"
			          ?>
			        </li>
			        <li class="nav-item">
			        	<?php
			          		echo "<a class='nav-link' href='cerrar_sesion.php?indice=$indice'>Cerrar Sesión</a>"
			          	?>
			        </li>			        
			      </ul>			      
			    </div>
			  </div>
			</nav>

		</div>
		<div class="container mw-100">

			<div class="pt-5">
				<h2>Usuario Autenticado</h2>
			</div>
			<div class="container mw-100 py-3">
				<table class="table table-bordered border-secondary">
					<thead class="bg-secondary bg-opacity-25">
						<tr>
							<th scope="col" class="fs-3">
								<?php
									echo $usuario[$indice]['nombre'] . " " . $usuario[$indice]['primer_apellido'];
								?>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr class="fs-4">
							<td>
								<p>Información</p>
								<p>Número de cuenta: <?php	echo $usuario[$indice]['numero_cuenta']; ?></p>
								<p>Fecha de Nacimiento: <?php	echo $usuario[$indice]['fecha_nac']; ?></p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pt-5">
				<h2>Datos guardados:</h2>
			</div>
			<div class="container mw-100 py-3">
				<table class="table table-bordered border-dark text-center" id="tabla">
					<thead>
						<tr>
							<th scope="col" class="fs-3">
								#
							</th>
							<th scope="col" class="fs-3">
								Nombre
							</th>
							<th scope="col" class="fs-3">
								Fecha de Nacimiemto
							</th>
						</tr>
					</thead>
					<tbody class="table-group-divider">
						<?php 
							
							$i = 1;
							$j = 1;
							//echo count($usuario);
							//echo count($usuario[$indice]);
							while($i <= count($usuario)){ 

								echo "<tr class='fs-4'>";
								while($j <= count($usuario[$indice])){

									
									if($j==1){

										echo "<th scope='row' colspan='1'>" . $usuario[$i]['numero_cuenta'] . "</th>";

									}else if($j==2){

										echo "<td colspan='1'>" . $usuario[$i]['nombre'] . " " . $usuario[$i]['primer_apellido'] . " " . $usuario[$i]['segundo_apellido'] . "</td>";
									}else if($j==7){

										echo "<td colspan='1'>" . $usuario[$i]['fecha_nac'] . "</td>";

									}
			
									$j++;
								}
								echo "</tr>";
								$j=1;
							    $i++;
							} 
						?>
					</tbody>
				</table>
			</div>

		</div>
	</body>
</html>
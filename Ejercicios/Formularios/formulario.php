<?php 

	session_start();
	if(!isset($_SESSION['usuario'])){
		header("location: login.php");
	}
	$indice = $_REQUEST['indice'];
	
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>Info</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>
	<body class="d-flex flex-column vh-100">
		<div class="container mw-100 px-0">

			<nav class="navbar navbar-expand-lg navbar-dark bg-primary mw-100">
			  <div class="container-fluid">
			    <?php
			        echo "<a class='navbar-brand' href='info.php?indice=$indice'>Home</a>"
			    ?>
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarText">
			      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
			        <li class="nav-item">
			          <?php
			          	echo "<a class='nav-link active' aria-current='page' href='formulario.php?indice=$indice'>Registrar Alumnos</a>"
			          ?>
			        </li>
			        <li class="nav-item">
			        	<?php
			          		echo "<a class='nav-link' href='cerrar_sesion.php?indice=$indice'>Cerrar Sesión</a>"
			          	?>
			        </li>			        
			      </ul>			      
			    </div>
			  </div>
			</nav>

		</div>
		<div class="container mw-100 pt-5">
			<form action="agregar.php" method="POST">
				<div class="mb-3 row">
				    <label for="numero_cuenta" class="col-sm-2 col-form-label">Número de cuenta</label>
				    <div class="col-sm-10 px-0">
				      <input type="number" class="form-control" id="numero_cuenta" name="numero_cuenta" placeholder="Número de cuenta">
			    	</div>
			  	</div>
			  	<div class="mb-3 row">
				    <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
				    <div class="col-sm-10 px-0">
				      <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
				    </div>
  				</div>
  				<div class="mb-3 row">
				    <label for="ap" class="col-sm-2 col-form-label">Primer Apellido</label>
				    <div class="col-sm-10 px-0">
				      <input type="text" class="form-control" id="ap" name="ap" placeholder="Primer apellido">
				    </div>
  				</div>
  				<div class="mb-3 row">
				    <label for="am" class="col-sm-2 col-form-label">Segundo Apellido</label>
				    <div class="col-sm-10 px-0">
				      <input type="text" class="form-control" id="am" name="am" placeholder="Segundo apellido">
				    </div>
  				</div>
  				<div class="mb-3 row">
				    <label for="genero" class="col-sm-2 col-form-label">Género</label>
				    <div class="col-sm-10 form-check">
				      	<input class="form-check-input" type="radio" name="genero" id="inputGenH" value="H">
  						<label class="form-check-label" for="inputGenH">
    						Hombre
  						</label>
  						<br>
  						<input class="form-check-input" type="radio" name="genero" id="inputGenM" value="M">
  						<label class="form-check-label" for="inputGenM">
    						Mujer
  						</label>
  						<br>
  						<input class="form-check-input" type="radio" name="genero" id="inputGenO" value="O">
  						<label class="form-check-label" for="inputGenO">
    						Otro
  						</label>
				    </div>				 
  				</div>
  				<div class="mb-3 row">
				    <label for="fecha_nac" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
				    <div class="col-sm-10 px-0">
				      <input type="text" class="form-control" id="fecha_nac" name="fecha_nac" placeholder="dd/mm/aa">
				    </div>
  				</div>
  				<div class="mb-3 row">
				    <label for="password" class="col-sm-2 col-form-label">Contraseña</label>
				    <div class="col-sm-10 px-0">
				      <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
				    </div>
  				</div>
  				<div class="mb-3 row mx-2">
				    <input type="submit" value="Registrar" class="mt-4 btn btn-success fw-bold fs-5">
  				</div>
  				<?php echo "<input type='hidden' id='indice' name='indice' value='$indice'>"; ?>
			</form>
		</div>
	</body>
</html>
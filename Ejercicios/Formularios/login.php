<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>Login</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</head>
<body class="d-flex justify-content-center align-items-center vh-100">
	<div class="bg-secondary bg-opacity-25 p-5 rounded-5">
		<div class="text-center fs-1 fw-bold">
			<h2>Login</h2>
		</div>
		<div>
			<div>
				<form action="autenticar.php" method="POST">
					<label for="cuenta" class="mt-4">Número de cuenta:</label>
					<input type="text" name="cuenta" class="form-control">

					<label for="password" class="mt-4">Contraseña:</label>
					<input type="password" name="password" class="form-control">

					<input type="submit" value="Entrar" class="mt-4 btn btn-primary fw-bold">
				<form>
			</div> 			
		</div>
		
	</div>
	
</body>
</html>
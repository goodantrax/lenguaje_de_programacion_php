<?php

	//Realizar una expresión regular que detecte emails correctos.

	$regex_email = '/^\\S+@\\S+\\.\\S+$/';
	$check = "castrejon@hotmail.com";
	//$check = "castrejon.com@hotmail";
	$result = preg_match($regex_email, $check);
	if($result == 1){

		echo "El email $check tiene el formato correcto<br><br>";

	}else{

		echo "El email $check no tiene el formato correcto<br><br>";

	}


	//Realizar una expresion regular que detecte Curps Correctos
	//ABCD123456EFGHIJ78.

	$regex_curp = '/^ABCD123456EFGHIJ78$/';
	$check2 = "ABCD123456EFGHIJ78";
	//$check2 = "AFCD123456EFGHIJ78";
	$result2 = preg_match($regex_curp, $check2);
	if($result2 == 1){

		echo "El CURP $check2 tiene el formato correcto<br><br>";

	}else{

		echo "El CURP $check2 no tiene el formato correcto<br><br>";

	}


	//Realizar una expresion regular que detecte palabras de longitud mayor a 50
	//formadas solo por letras.

	$regex_palabra = '/^[a-zA-z]{50,}$/';
	$check3 = "ABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJ";
	//$check3 = "AFCD123456EFGHIJ78";
	$result3 = preg_match($regex_palabra, $check3);
	if($result3 == 1){

		echo "La palabra $check3 tiene el formato correcto<br><br>";

	}else{

		echo "La palabra $check3 no tiene el formato correcto<br><br>";

	}


	//Crea una funcion para escapar los simbolos especiales.
	$cadena = "El producto tiene un precio de $10 pero se divide a la mitad por el 50% de descuento, osea {10/2 = $5}";
	$cadena_escapada = preg_quote($cadena, '/');
	echo "La cadena es: $cadena<br>";
	echo "La cadena escapada es: $cadena_escapada<br><br>";


	//Crear una expresion regular para detectar números decimales.
	$regex_decimal = '/^[0-9]+[.][0-9]+$/';
	$check4 = "1024.34";
	//$check4 = "1024";
	$result4 = preg_match($regex_decimal, $check4);
	if($result4 == 1){

		echo "El número $check4 es decimal<br><br>";

	}else{

		echo "El número $check4 no es decimal<br><br>";

	}


?>